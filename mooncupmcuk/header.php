<?php
/**
 * Mooncup Main template for displaying the header
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 */
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="ie ie-no-support" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="ie ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="ie ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9]>         <html class="ie ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="robots" content="noindex">
	<title><?php wp_title( ); ?></title>
	<meta name="viewport" content="width=device-width" />
	<!--[if lt IE 9]>
		<script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
	<script src="https://use.fontawesome.com/b2f1ff9581.js"></script>
</head>

<body <?php body_class(); ?>>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.6";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	
	<div class="site">
		<header class="site-header">
			<div class="master-nav">
				<span style="float:left"><?php _e('Delivery: ','woocommerce'); ?> </span><?php do_action('wcpbc_manual_country_selector'); ?>


				<div class="language"> <label> <?php _e('Language: ','woocommerce'); ?> </label>
					<?php do_action('wpml_add_language_selector');?>
				</div>
            
				<div class="nav-links-head">
					<a href="javascript:void(0)" class="navigation-menu-button" id="js-mobile-menu-small"><span class="fa fa-bars"></span></a>
					<div class="navigation-menu-small">
						<?php
						wp_nav_menu( array('menu' => 'top-right-menu' ));
						?>
					</div>
				</div>
			</div><!--end of smaller top nav-->

			<div class="primary-nav">
				<?php if ( get_theme_mod( 'mooncupmain_logo' ) ) : ?>
			    <div class='site-logo'>
			        <a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img src='<?php echo esc_url( get_theme_mod( 'mooncupmain_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a>
			    </div>
				<?php else : ?>
			    <hgroup>
			        <h1 class='site-title'><a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><?php bloginfo( 'name' ); ?></a></h1>
			        <h2 class='site-description'><?php bloginfo( 'description' ); ?></h2>
			    </hgroup>
				<?php endif; ?>

				<div class="navigation">
					<div class="navigation-wrapper">

					    <a href="javascript:void(0)" class="navigation-menu-button" id="js-mobile-menu"><span class="fa fa-bars"></span></a>

						<nav role="navigation"><?php
							$nav_menu = wp_nav_menu(
								array(
									'menu'              => 'Main Menu',
					                //'theme_location'    => 'primary',
					                'depth'             => 2,
									'container' => 'ul id="js-navigation-menu"',
									'container_class' => 'navigation-menu show',
									'container_id'      => 'js-navigation-menu',
				                	'menu_class'        => 'navigation-menu show',
				                	'$wrap_id'        => 'js-navigation-menu',
									'items_wrap' => '<ul class="%2$s">%3$s</ul>',
									'fallback_cb' => '__return_false',
								)
							); ?>
						</nav>
					</div>
				</div><!--end of main navigation-->
			</div><!-- end of primary nav container-->
		</header>