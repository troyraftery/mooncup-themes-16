<?php
/**
 * Mooncup Main template for displaying the footer
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 */
?>
				<div class="pre-footer container_boxed">
					 <?php echo do_shortcode('[gallery link="none" ids="237,211,185,159" columns="4"]'); ?>
				</div>

				<div class="mailing-list bg-blue text-reverse">
					<div class="container_boxed">
						<div class="mcwp-form white-lined">
							<?php sutb_form_captureform(false);?>
						</div>
					</div>
				</div>
				<footer class="footer">
					<ul class="footer-widgets container_boxed"><?php
						if ( function_exists( 'dynamic_sidebar' ) ) :
							dynamic_sidebar( 'footer-sidebar' );
						endif; ?>
					</ul>

					<div class="credits-container">
						<div class="container_boxed">
							<div class="credits-links">
								<?php
								wp_nav_menu( array('menu' => 'footer-terms' ));
								?>
							</div>
							<div class="credits-neptik">
								<p>&copy; Copyright 2002-<?php echo date("Y");?> Mooncup Ltd / Site designed and developed by <a href="http://www.freshrecipe.co.uk/" title="Fresh Recipe" target="_blank">Neptik</a></p>
							</div>
						</div>
					</div>
				</footer>

			</div><!--end of site-->

			<div id="modal" style="display: none">
				<div id="startDialog">
    				<div>
    				<?php
    					$country = new WC_Countries;
    					$countries = $country->get_allowed_countries();
    				?>
    				<form method="POST" action="" >

    					<select id="location" name="location"  onChange="jumpMenu('parent',this,0)" >
    					<?php
    					if(isset($_GET['geocode'])):
    							$_COOKIE['location'] = $_GET['geocode'];
    					endif;
							echo '<option value="" selected>';
						_e('Select your delivery country','mooncupmail');
						echo '</option>';
    					foreach($countries as $key =>$value) {

    				      echo '<option value="'.$key.'">'.$value.'</option>';

    				    }
						echo '<option value="??">';
						_e('Other');
						echo '</option>';
    					?>
    					</select>
    					</form>
    				</div>
				</div>
			</div>
			<div id="changemodal" style="display: none"><h2>Changing your delivery country will reset your basket</h2>
				<div id="startDialog">
					<div>
						<?php
							$country = new WC_Countries;
							$countries = $country->get_allowed_countries();
						?>

						</div>
				</div>
			</div>
			<script>
			function jumpMenu(targ,selObj,restore){ //v3.0

				var location = selObj.options[selObj.selectedIndex].value;
				if(location == 'UK' || location == 'FR' || location == 'DE' || location == 'NL' || location == 'SE' || location == 'PT-PT' || location == 'FM' || location == 'MP' || location == 'PR' || location == 'VI'){
					document.location.href = '/';
				}else if(location == 'IL'){
					document.location.href = '/israel/';
				}else if(location == 'HR'){
					document.location.href = '/croatia/';
				}else if(location == 'IT'){
					document.location.href = '/italy/';
				}else{

					if(document.cookie.indexOf("woocommerce_cart_hash") >= 0){
						var date = new Date();
						date.setTime(date.getTime()-(2*86400*1000));
						document.cookie = 'locationchange =; expires='+date.toGMTString()+';path=/;';
						var date = new Date();
						date.setTime(date.getTime()+(2*86400*1000));
						document.cookie = 'locationchange ='+location+'; expires='+date.toGMTString()+';path=/;';

						jQuery('#changemodal').dialog({
						 resizable: false,
						 height:240,
						 width: 600,
						 modal: true,
						 buttons: {
						 "Change delivery country": function() {
						 old_url = window.location.href;
						 _url = old_url.substring(0, old_url.indexOf('?'));
						 document.location.href = '/<?php echo ICL_LANGUAGE_CODE; ?>/?p=3545&geocode=' + location+'&empty-cart=true';
						 var date = new Date();
						 date.setTime(date.getTime()+(2*86400*1000));
						 document.cookie = 'location ='+location+'; expires='+date.toGMTString()+';path=/;';
						 jQuery( this ).dialog( "close" );
						 },
						 Cancel: function() {
						 window.location.reload();
						 jQuery( this ).dialog( "close" );
						 }
						 }
						 });
					}else{
						old_url = window.location.href;
						_url = old_url.substring(0, old_url.indexOf('?'));
						document.location.href = '/<?php echo ICL_LANGUAGE_CODE; ?>/?p=3545&geocode=' + location;
						var date = new Date();
						date.setTime(date.getTime()+(2*86400*1000));
						document.cookie = 'location ='+location+'; expires='+date.toGMTString()+';path=/;';
					}
				}
			}
			jQuery('a.modal-activate,li.modal-activate').on('click',function(e){
				if (window.location.href.indexOf("checkout/order-received") >= 0) {
				}else{
					var link = this;
					e.preventDefault();

					/*jQuery('#startDialog').dialog({
					 resizable: false,
					 height:175,
					 width: 600,
					 modal: true,
					 title: "Select your delivery country",
					 /*buttons: {
					 "Ok": function() {
					 old_url = window.location.href;
					 _url = old_url.substring(0, old_url.indexOf('?'));
					 document.location.href = '/buy-the-mooncup?geocode=' + location;
					 var date = new Date();
					 date.setTime(date.getTime()+(2*86400*1000));
					 document.cookie = 'location ='+location+'; expires='+date.toGMTString()+';path=/;';
					 },
					 "Cancel": function() {
					 $(this).dialog("close");
					 }
					 }
					 });*/
				}
			});
			</script>

		<?php wp_footer(); ?>

	</body>
</html>
