<?php
/**
 * Mooncup Main template for displaying Single-Posts
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 */

get_header(); ?>
	
	<section id="blog-single" class="blog page-content primary" role="main">
		<div class="container_full">
			<?php $my_query = new WP_Query( array('pagename' => 'mooncup-blog', 'showposts' => '999')); ?>
			<?php if ($my_query->have_posts()): ?>
			<?php while ( $my_query->have_posts() ) : $my_query->the_post(); ?>	
			<?php

			/*
			*  Loop through post objects (assuming this is a multi-select field) ( setup postdata )
			*  Using this method, you can use all the normal WP functions as the $post object is temporarily initialized within the loop
			*  Read more: http://codex.wordpress.org/Template_Tags/get_posts#Reset_after_Postlists_with_offset
			*/

			$post_objects = get_field('post_objects');

			if( $post_objects ): ?>
			
			    <ul class="blog-slider">
			    <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
			        <?php setup_postdata($post); ?>
			        <li>
			        	<div class="blog-slide">
			        		<div class="blog-slide--image">
					        	<?php if (has_post_thumbnail( $post->ID ) ): ?>
								<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
					        		<div class = "content post-featured-image image-cover" style="background-image:url('<?php echo $image[0]; ?>');"></div>
						    	<?php endif; ?>
					        </div>
			        		<div class="blog-slide--content">
					            <a href="<?php the_permalink(); ?>"><h1><?php the_title(); ?></h1></a>
					            <span><?php the_excerpt()?></span>
					            <a href="<?php the_permalink(); ?>" class="btn-black">Read more</a>
					        </div>
					        
			        	</div>
			        </li>
			    <?php endforeach; ?>
			    </ul>
			
			    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php endif;

		

			?>
			<?php endwhile;?>
			<?php endif;?>
			<?php wp_reset_query(); ?>
		</div>
		<div class="container_boxed content_band--small no-pad-top">
			<div class="container__full blog-utility">
				
				<div class="blog-utility--item breadcrumbs">
					<?php if ( function_exists('yoast_breadcrumb') ) 
					{yoast_breadcrumb('<span id="breadcrumbs">','</span>');} ?>
				</div>

				<div class="align-right">
					<div class="blog-utility--item search">
						<?php include (TEMPLATEPATH . '/searchform.php'); ?>
					</div>

					<div class="blog-utility--item social">
						<?php $my_query = new WP_Query( array('pagename' => 'mooncup-blog', 'showposts' => '999')); ?>
						<?php if ($my_query->have_posts()): ?>
						<?php while ( $my_query->have_posts() ) : $my_query->the_post(); ?>	
						<ul>
							<li class="social-icon"><a href="<?php the_field('facebook_link'); ?>" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
							<li class="social-icon"><a href="<?php the_field('twitter_link'); ?>" target="_blank"><i class="fa fa-twitter-square"></i></a></li>
							<li class="social-icon"><a href="<?php the_field('google_link'); ?>" target="_blank"><i class="fa fa-google-plus-square"></i></a></li>
							<li class="social-icon"><a href="<?php the_field('youtube_link'); ?>" target="_blank"><i class="fa fa-youtube-square"></i></a></li>
							<li class="social-icon"><a href="<?php the_field('pinterest_link'); ?>" target="_blank"><i class="fa fa-pinterest-square"></i></a></li>
							<li class="social-icon"><a href="<?php the_field('instagram_link'); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
						</ul>
						<?php endwhile;?>
						<?php endif;?>
						<?php wp_reset_query(); ?>
					</div>
				</div>
			</div>

			<ul class="nav--blog-category">
			    <?php wp_list_categories( array(
			    	'taxonomy' => 'blogs',
			        'orderby' => 'name',
			        'title_li' => '',
			        'exclude' => '129'
			    ) ); ?> 
			</ul>

 			<article class="blog-single blog-post col__8">
					<?php
						if ( have_posts() ) : the_post();?>
						<h1 class="post-title center"><?php the_title();?></h1>
						<div class="blog-image">
							<?php if (has_post_thumbnail( $post->ID ) ): ?>
							<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
				        		<div class = "content post-featured-image image-cover" style="background-image:url('<?php echo $image[0]; ?>');"></div>
					    	<?php endif; ?>
				    	</div>

						<div class="share-links alignright">
							Share it
							<a href="http://twitter.com/home/?status=<?php the_title(); ?> - <?php the_permalink(); ?>" title="Tweet this!" target="_blank"><i class="fa fa-lg fa-twitter-square"></i></a>
							<a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>" title="Share on Facebook" target="_blank"><i class="fa fa-lg fa-facebook-square"></i></a>
							<a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" onclick="javascript:window.open(this.href,
		  						'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-lg fa-google-plus-square"></i></a>
		  					<a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); echo $url; ?>" target="_blank"><i class="fa fa-lg fa-pinterest-square"></i></a>
		  					<a href="mailto:?subject=Mooncup Article <?php the_title(); ?>&amp;body=Check out this article <?php the_permalink(); ?>"title="Share by Email"><i class="fa fa-lg fa-envelope-square"></i></a>
						</div>

						<div class="post-info small caps">
							<?php the_author(); ?>
						</div>
						
						<div class="container_full post-content">	
							<?php the_content();?>

							<aside class="post-aside">

								<div class="post-links">
									<?php previous_post_link( '%link', __( '&laquo; Previous post', 'mooncupmain' ) ) ?>
									<?php next_post_link( '%link', __( 'Next post &raquo;', 'mooncupmain' ) ); ?>
								</div>

									<?php
										if ( comments_open() || get_comments_number() > 0 ) :
											comments_template( '', true );
										endif;
									?>

							</aside>
							<?php
								else :
									get_template_part( 'loop', 'empty' );
								endif;
							?>
						</div>
					

			</article>

			<aside class="blog-sidebar col__4">
	        	<ul class="blog-sidebar"><?php
					if ( function_exists( 'dynamic_sidebar' ) ) :
						dynamic_sidebar( 'blog-sidebar' );
					endif; ?>
				</ul>	
	        </aside>
	   	</div>

	</section>
<?php get_footer(); ?>