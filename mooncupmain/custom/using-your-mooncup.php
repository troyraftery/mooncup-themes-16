<?php
/**
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 * Template Name: Using your mooncup LP
 */

get_header(); ?>
<section class="single-col using-your-mooncup page-content primary" role="main">

	        <article class="container_full splash-content-block">
	        	<div class = "splash-image-narrow splash-image_generic image_fullwidth" style="background-image:url('<?php the_field('splash_image'); ?>');">
		        	<div class="splash-content-overlay splash-header text-reverse">
		        		<div class="container_full">
			        	<?php the_field('splash_content'); ?>
			        	</div>
		        	</div>
		        </div>
		    </article>

	        	<section class="container_boxed content_band">
	        		<aside class="sidebar col__4">
	        		<ul class="sidebar"><?php
						if ( function_exists( 'dynamic_sidebar' ) ) :
							dynamic_sidebar( 'faq-sidebar' );
						endif; ?>
					</ul>
	        		</aside>



	        		<article class="faq-content col__8">
	        			<div class="container">
	        				

	        			<?php the_field('1col_content_area');?>
	        			</div>

		        		<div class="faq-cat-tiles">
		        		<?php

						// check if the repeater field has rows of data
						if( have_rows('using_mc_tile') ):?>
							
						    <?php while ( have_rows('using_mc_tile') ) : the_row();?>
			
							
							<div class="image-grid-block">
								
							        	<a href="/<?php the_sub_field('tile_link_destination');?>" >
								        	<div class = "image-grid-item" style="background:linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url('<?php the_sub_field('grid_image'); ?>');">
									        	<div class="grid-content-container center text-reverse">
									        		<div class="grid-content">
										        	<h2><?php
										        	the_sub_field('tile_text');
										        	?>
										        	</h2>
										        	</div>
									        	</div>
									        </div>
								    	</a>
							
							</div>
						       

						    <?php endwhile;?>
						    
						<?php 

						else :

						    // no rows found

						endif;

						?>
		        		</div>
	        		</article>

	        		

	        	</section>

</section>
<!--  Here 
<?php
//list terms in a given taxonomy (useful as a widget for twentyten)
$taxonomy = 'usage';
$tax_terms = get_terms($taxonomy);
?>
<ul>
<?php
foreach ($tax_terms as $tax_term) {

					  echo '<a href="'.esc_attr(get_term_link($tax_term, $taxonomy)).'" rel="bookmark">';
            echo '<div class="post-image" style="background: url('.$tax_term->slug.'.png) no-repeat center;background-size: cover;height:200px;"></div>';
            echo '<div class="post-info-container">
                <h2 class="post-title">';
                    if ( is_singular() ) :
                        echo $tax_term->name;
                    else :
                        echo $tax_term->name;
                    endif;
                echo '</h2></a>';
}
?>
</ul> -->
<?php get_footer(); ?>
