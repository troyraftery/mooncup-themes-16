<?php
/**
 * Mooncup Main functions file
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 */


/******************************************************************************\
	Theme support, standard settings, menus and widgets
\******************************************************************************/

add_theme_support( 'post-formats', array( 'image', 'quote', 'status', 'link' ) );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'automatic-feed-links' );
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

$custom_header_args = array(
	'width'         => 980,
	'height'        => 300,
	'default-image' => './images/header.png',
);
add_theme_support( 'custom-header', $custom_header_args );

/**
 * Print custom header styles
 * @return void
 */
function mooncupmain_custom_header() {
	$styles = '';
	if ( $color = get_header_textcolor() ) {
		echo '<style type="text/css"> ' .
				'.site-header .logo .blog-name, .site-header .logo .blog-description {' .
					'color: #' . $color . ';' .
				'}' .
			 '</style>';
	}
}
add_action( 'wp_head', 'mooncupmain_custom_header', 11 );

$custom_bg_args = array(
	'default-color' => 'fba919',
	'default-image' => '',
);
add_theme_support( 'custom-background', $custom_bg_args );

register_nav_menu( 'main-menu', __( 'Your sites main menu', 'mooncupmain' ) );

if ( function_exists( 'register_sidebars' ) ) {
	register_sidebar(
		array(
			'id' => 'blog-sidebar',
			'name' => __( 'Blog widgets', 'mooncupmain' ),
			'description' => __( 'Shows on blog page', 'mooncupmain' )
		)
	);

	register_sidebar(
		array(
			'id' => 'footer-sidebar',
			'name' => __( 'Footer widgets', 'mooncupmain' ),
			'description' => __( 'Shows in the sites footer', 'mooncupmain' )
		)
	);
	register_sidebar(
		array(
			'id' => 'faq-sidebar',
			'name' => __( 'faq widgets', 'mooncupmain' ),
			'description' => __( 'Shows in the faqs', 'mooncupmain' )
		)
	);
    register_sidebar(
        array(
            'id' => 'testimonials-sidebar',
            'name' => __( 'testimonials widgets', 'mooncupmain' ),
            'description' => __( 'Shows in the testimonials sidebar', 'mooncupmain' )
        )
    );
}

if ( ! isset( $content_width ) ) $content_width = 650;

/**
 * Include editor stylesheets
 * @return void
 */
//function mooncupmain_editor_style() {
   // add_editor_style( 'css/wp-editor-style.css' );
//}
//add_action( 'init', 'mooncupmain_editor_style' );


/******************************************************************************\
	Scripts and Styles
\******************************************************************************/

/**
 * Enqueue mooncupmain scripts
 * @return void
 */
function mooncupmain_enqueue_scripts() {
	wp_enqueue_style( 'mooncupmain-styles', get_stylesheet_uri(), array(), '1.0' );
	wp_enqueue_style( 'jqueryui-styles', 'https://code.jquery.com/ui/1.11.4/themes/humanity/jquery-ui.css', array(), '1.0' );
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'jquery-ui', 'https://code.jquery.com/ui/1.11.4/jquery-ui.min.js', array(), '1.0', true );
	wp_enqueue_script( 'slider-scripts', get_template_directory_uri() . '/js/jquery.bxslider.min.js', array(), '1.0', true );
	wp_enqueue_script( 'default-scripts', get_template_directory_uri() . '/js/scripts.min.js', array(), '1.0', true );
	wp_enqueue_script( 'retina-scripts', get_template_directory_uri() . '/js/retina.min.js', array(), '1.0', true );


	if ( is_singular() ) wp_enqueue_script( 'comment-reply' );

}
add_action( 'wp_enqueue_scripts', 'mooncupmain_enqueue_scripts' );


/******************************************************************************\
	Content functions
\******************************************************************************/

/**
 * Displays meta information for a post
 * @return void
 */
function mooncupmain_post_meta() {
	if ( get_post_type() == 'post' ) {
		echo sprintf(
			__( 'Posted %s in %s%s by %s. ', 'mooncupmain' ),
			get_the_time( get_option( 'date_format' ) ),
			get_the_category_list( ', ' ),
			get_the_tag_list( __( ', <b>Tags</b>: ', 'mooncupmain' ), ', ' ),
			get_the_author_link()
		);
	}
	edit_post_link( __( ' (edit)', 'mooncupmain' ), '<span class="edit-link">', '</span>' );
}

//custom logo
function mooncupmain_theme_customizer( $wp_customize ) {
    $wp_customize->add_section( 'mooncupmain_logo_section' , array(
    'title'       => __( 'Logo', 'mooncupmain' ),
    'priority'    => 30,
    'description' => 'Upload a logo to replace the default site name and description in the header',
	) );
	$wp_customize->add_setting( 'mooncupmain_logo' );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'mooncupmain_logo', array(
    'label'    => __( 'Logo', 'mooncupmain' ),
    'section'  => 'mooncupmain_logo_section',
    'settings' => 'mooncupmain_logo',
	) ) );
}

add_action( 'customize_register', 'mooncupmain_theme_customizer' );

// Callback function to insert 'styleselect' into the $buttons array
function my_mce_buttons_2( $buttons ) {
array_unshift( $buttons, 'styleselect' );
return $buttons;
}
// Register our callback to the appropriate filter
add_filter('mce_buttons_2', 'my_mce_buttons_2');

// Callback function to filter the MCE settings
function my_mce_before_init_insert_formats( $init_array ) {
// Define the style_formats array
$style_formats = array(
// Each array child is a format with it's own settings
array(
'title' => 'Pink solid button',
'selector' => 'a',
'classes' => 'btn-primary'
)
);
// Insert the array, JSON ENCODED, into 'style_formats'
$init_array['style_formats'] = json_encode( $style_formats );

return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );



/*
* wc_remove_related_products
*
* Clear the query arguments for related products so none show.
* Add this code to your theme functions.php file.
*/
function wc_remove_related_products( $args ) {
  return array();
}

add_filter('woocommerce_related_products_args','wc_remove_related_products', 10);

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['description'] );      	// Remove the description tab
    unset( $tabs['reviews'] ); 			// Remove the reviews tab
    unset( $tabs['additional_information'] );  	// Remove the additional information tab

    return $tabs;

}

// Change country selector and set default country
add_filter( 'default_checkout_country', 'change_default_checkout_country' );
function change_default_checkout_country() {
  if(isset($_GET['geocode'])):
      $location = $_GET['geocode'];
    elseif(isset($_COOKIE['location'])):
      $location = $_COOKIE['location'];
    else:
        $location = 'fr';
    endif;
    return $location;
}



// Custom post-types and taxonomies

function custom_post_usage() {
	$labels = array(
		'name'               => _x( 'Using Mooncup Articles', 'post type general name' ),
		'singular_name'      => _x( 'Article', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'usage' ),
		'add_new_item'       => __( 'Add New Article' ),
		'edit_item'          => __( 'Edit Article' ),
		'new_item'           => __( 'New Article' ),
		'all_items'          => __( 'All Articles' ),
		'view_item'          => __( 'View Article' ),
		'search_items'       => __( 'Search Articles' ),
		'not_found'          => __( 'No articles found' ),
		'not_found_in_trash' => __( 'No articles found in the Trash' ),
		'parent_item_colon'  => 'Posts',
		'menu_name'          => 'Using Mooncup'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => 'Holds the articles data',
		'public'        => true,
	        'menu_position' => 30,
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'post-formats' , 'tags'),
		'has_archive'   => true,
		'rewrite' => array( 'slug' => 'using-mooncup', 'with_front' => true ),
		'taxonomies' => array('post_tag')
	);
	register_post_type( 'using-mooncup', $args );
	flush_rewrite_rules( false );
}
function taxonomies_usage() {
	$labels = array(
		'name'              => _x( 'Usage Categories', 'taxonomy general name' ),
		'singular_name'     => _x( 'Usage', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Categories' ),
		'all_items'         => __( 'All Categories' ),
		'parent_item'       => __( 'Parent Category' ),
		'parent_item_colon' => __( 'Parent Category:' ),
		'edit_item'         => __( 'Edit Category' ),
		'update_item'       => __( 'Update Category' ),
		'add_new_item'      => __( 'Add New Category' ),
		'new_item_name'     => __( 'New Category' ),
		'menu_name'         => __( 'Categories' ),
	);
	$args = array(
		'labels' => $labels,
		'hierarchical' => true,
		'has_archive'   => true,
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'post-formats', 'tags' )
	);
	 if(  get_current_blog_id()==3 ) $args['rewrite'] =  array( 'slug' => 'domande-frequenti', 'with_front' => true );
	register_taxonomy( 'usage', 'using-mooncup', $args );
}
function custom_post_testimonial() {
    $labels = array(
        'name'               => _x( 'Testimonial Articles', 'post type general name' ),
        'singular_name'      => _x( 'Article', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'testimonial' ),
        'add_new_item'       => __( 'Add New Article' ),
        'edit_item'          => __( 'Edit Article' ),
        'new_item'           => __( 'New Article' ),
        'all_items'          => __( 'All Articles' ),
        'view_item'          => __( 'View Article' ),
        'search_items'       => __( 'Search Articles' ),
        'not_found'          => __( 'No articles found' ),
        'not_found_in_trash' => __( 'No articles found in the Trash' ),
        'parent_item_colon'  => 'Posts',
        'menu_name'          => 'Testimonials'
    );
    $args = array(
        'labels'        => $labels,
        'description'   => 'Holds the magazine articles data',
        'public'        => true,
        'menu_position' => 30,
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'post-formats' , 'tags'),
        'has_archive'   => true,
        'rewrite' => array( 'slug' => 'testimonial', 'with_front' => true ),
        'taxonomies' => array('post_tag')
    );
    register_post_type( 'testimonial', $args );
    flush_rewrite_rules( false );
}
function taxonomies_testimonial() {
    $labels = array(
        'name'              => _x( 'Testimonial Categories', 'taxonomy general name' ),
        'singular_name'     => _x( 'Testimonial', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Categories' ),
        'all_items'         => __( 'All Categories' ),
        'parent_item'       => __( 'Parent Category' ),
        'parent_item_colon' => __( 'Parent Category:' ),
        'edit_item'         => __( 'Edit Category' ),
        'update_item'       => __( 'Update Category' ),
        'add_new_item'      => __( 'Add New Category' ),
        'new_item_name'     => __( 'New Category' ),
        'menu_name'         => __( 'Categories' ),
    );
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'has_archive'   => true,
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'post-formats', 'tags' )
    );
    register_taxonomy( 'testimonials', 'testimonial', $args );
}
function custom_post_blog() {
    $labels = array(
        'name'               => _x( 'Blog Articles', 'post type general name' ),
        'singular_name'      => _x( 'Article', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'blog' ),
        'add_new_item'       => __( 'Add New Article' ),
        'edit_item'          => __( 'Edit Article' ),
        'new_item'           => __( 'New Article' ),
        'all_items'          => __( 'All Articles' ),
        'view_item'          => __( 'View Article' ),
        'search_items'       => __( 'Search Articles' ),
        'not_found'          => __( 'No articles found' ),
        'not_found_in_trash' => __( 'No articles found in the Trash' ),
        'parent_item_colon'  => 'Posts',
        'menu_name'          => 'Blogs'
    );
    $args = array(
        'labels'        => $labels,
        'description'   => 'Holds the magazine articles data',
        'public'        => true,
        'menu_position' => 30,
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'post-formats' , 'tags'),
        'has_archive'   => true,
        'rewrite' => array( 'slug' => 'blog', 'with_front' => true ),
        'taxonomies' => array('post_tag')
    );
    register_post_type( 'blog', $args );
    flush_rewrite_rules( false );
}
function taxonomies_blog() {
    $labels = array(
        'name'              => _x( 'Blog Categories', 'taxonomy general name' ),
        'singular_name'     => _x( 'Blog', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Categories' ),
        'all_items'         => __( 'All Categories' ),
        'parent_item'       => __( 'Parent Category' ),
        'parent_item_colon' => __( 'Parent Category:' ),
        'edit_item'         => __( 'Edit Category' ),
        'update_item'       => __( 'Update Category' ),
        'add_new_item'      => __( 'Add New Category' ),
        'new_item_name'     => __( 'New Category' ),
        'menu_name'         => __( 'Categories' ),
    );
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'has_archive'   => true,
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'post-formats', 'tags' )
    );
    register_taxonomy( 'blogs', 'blog', $args );
}
function custom_post_stockists() {
    $labels = array(
        'name'               => _x( 'Stockists', 'post type general name' ),
        'singular_name'      => _x( 'Stockist', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'stockists' ),
        'add_new_item'       => __( 'Add New Stockist' ),
        'edit_item'          => __( 'Edit Stockist' ),
        'new_item'           => __( 'New Stockist' ),
        'all_items'          => __( 'All Stockists' ),
        'view_item'          => __( 'View Stockist' ),
        'search_items'       => __( 'Search Stockists' ),
        'not_found'          => __( 'No stockists found' ),
        'not_found_in_trash' => __( 'No stockists found in the Trash' ),
        'parent_item_colon'  => 'Posts',
        'menu_name'          => 'Online Stockists'
    );
    $args = array(
        'labels'        => $labels,
        'description'   => 'Holds the stockists data',
        'public'        => true,
        'menu_position' => 30,
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'post-formats' , 'tags'),
        'has_archive'   => true,
        'rewrite' => array( 'slug' => 'stockists', 'with_front' => true ),
        'taxonomies' => array('post_tag')
    );
    register_post_type( 'stockists', $args );
    flush_rewrite_rules( false );
}
function taxonomies_onlinestockist() {
    $labels = array(
        'name'              => _x( 'Stockist Categories', 'taxonomy general name' ),
        'singular_name'     => _x( 'Stockist', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Categories' ),
        'all_items'         => __( 'All Categories' ),
        'parent_item'       => __( 'Parent Category' ),
        'parent_item_colon' => __( 'Parent Category:' ),
        'edit_item'         => __( 'Edit Category' ),
        'update_item'       => __( 'Update Category' ),
        'add_new_item'      => __( 'Add New Category' ),
        'new_item_name'     => __( 'New Category' ),
        'menu_name'         => __( 'Categories' ),
    );
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'has_archive'   => true,
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'post-formats', 'tags' )
    );
    register_taxonomy( 'online-stockists', 'stockists', $args );
}
add_action( 'init', 'custom_post_usage' );
add_action( 'init', 'custom_post_testimonial' );
add_action( 'init', 'custom_post_blog' );
add_action( 'init', 'custom_post_stockists' );
add_action( 'init', 'taxonomies_blog', 0 );
add_action( 'init', 'taxonomies_usage', 0 );
add_action( 'init', 'taxonomies_testimonial', 0 );
add_action( 'init', 'taxonomies_onlinestockist', 0 );



add_filter( 'embed_oembed_html', 'custom_youtube_oembed' );
function custom_youtube_oembed( $code ){
    if( stripos( $code, 'youtube.com' ) !== FALSE && stripos( $code, 'iframe' ) !== FALSE )
        $code = str_replace( '<iframe', '<iframe class="youtube-player" type="text/html" ', $code );

    return $code;
}
add_action('wp_logout',create_function('','wp_redirect(home_url());exit();'));

add_filter('single_add_to_cart_text', 'woo_custom_cart_button_text');
add_filter('add_to_cart_text', 'woo_custom_cart_button_text');
function woo_custom_cart_button_text() {
	return __('Buy Now', 'woocommerce');
}
function woocommerce_button_proceed_to_checkout() {
       $checkout_url = WC()->cart->get_checkout_url();
       ?>
       <a href="<?php echo $checkout_url; ?>" class="checkout-button button alt wc-forward"><?php _e( 'Buy Now', 'woocommerce' ); ?></a>
       <?php
     }
add_filter( 'woocommerce_order_button_text', create_function( '', 'return "Buy Now";' ) );
add_action( 'init', 'dist_shortcode' );

function dist_shortcode() {

	add_shortcode( 'dist-login-form', 'dist_login_form_shortcode' );
}
function dist_login_form_shortcode() {

    $user = wp_get_current_user();
    if ( in_array( 'distributor', (array) $user->roles ) ) {
        header('Location: /trade-distributor');
    }
	return wp_login_form( array( 'echo' => false ) );
}



/**
 * Add the field to the checkout
 */
/*
add_action( 'woocommerce_after_billing_state', 'my_custom_checkout_field' );

function my_custom_checkout_field( $checkout ) {

    echo '<div id="my_custom_checkout_field"><h2>' . __('My Field') . '</h2>';

    woocommerce_form_field( 'my_field_name', array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => __('How did you hear about us'),
        'placeholder'   => __('Tell us'),
        ), $checkout->get_value( 'my_field_name' ));

    echo '</div>';

}
/**
 * Update the order meta with field value

add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta' );

function my_custom_checkout_field_update_order_meta( $order_id ) {
    if ( ! empty( $_POST['my_field_name'] ) ) {
        update_post_meta( $order_id, 'My Field', sanitize_text_field( $_POST['my_field_name'] ) );
    }
}
/**
 * Display field value on the order edit page

add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );

function my_custom_checkout_field_display_admin_order_meta($order){
    echo '<p><strong>'.__('My Field').':</strong> ' . get_post_meta( $order->id, 'My Field', true ) . '</p>';
}/**/


/**
 * Filter the except length to 20 characters.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    return 40;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

//* Remove [...] from WordPress excerpts
function customize_excerpt_more( $more ) {
    return '...';
}
add_filter('excerpt_more', 'customize_excerpt_more');